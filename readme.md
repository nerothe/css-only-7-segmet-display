# CSS-Only 7-Segment-Display

![DRAMATIC_GIF](7sd.gif)

This is a 7-Segment-Display that only uses CSS.  
This could be useful, if you just don't want to use JS for your project and only want to use this for a static display.  
It can also be used with JS to make counters and other stuff.

## Configuration
Fully configurable in the `:root` section inside the [7sd.css](7sd.css):

```css
:root {
    --bg-color: none; /* background of this 7-segment display */
    --ia-color: #666; /* color of an inactive segment */
    --a-color: rgb(82, 192, 219); /* color of an active segment */
    --padding-p: 4px; /* padding between border and the segment */
    --bar-w: 16px; /* width of a segment */
    --bar-pad: 8px; /* this is an additional padding to the vertical edges */

    /* ... */
}
```

```html
<div class='s7s'>
    <input value=' ' hidden/> <!-- value of the segment -->
    <seg></seg>
    <seg></seg>
    <seg></seg>
    <seg></seg>
    <seg></seg>
    <seg></seg>
    <seg></seg>
</div>
```

## Future
This could easily be adapted to a bigger display — so that digits and letters could be displayed.